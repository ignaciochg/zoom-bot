from ics import Calendar
import requests
import os
# os.environ['DISPLAY'] = ':0'

import time
import re
import sys
import subprocess


# add calendar ics url here
url = 'https://calendar.google.com/calendar/ical/c8f09975222c1078ab68975ce3ed3c9c3ef564a8954563d15bfdcf2a5ca3a5e6%40group.calendar.google.com/private-bea18b3ea8694e2298ae48ca5d912a83/basic.ics'

processes = []
def dispatch(etime,ename,link):
   if int(etime) < int(time.time()):
      print("Event passed, skipping:", ename)
      return False
   p = subprocess.Popen([sys.executable, 'join-zoom.py', str(etime), link, ename])#, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
   processes.append(p)
   return True
   # os.system(f"{sys.executable} join-zoom.py '{etime}' '{link}' '{ename}' &")

zoom_regex = r'https://unr\.zoom\.us/j/\d*\?pwd=[a-zA-Z\d\.]*\w'
# zoom_regex = r'https://[a-zA-Z0-9]+\.zoom\.us/j/\d*\?pwd=\S*\w'

# https://lisper.in/fork-exec-python

c = Calendar(requests.get(url).text)

# # print(c.events)
# for e in c.events:
#  print(e.name)
# print("##########")

for e in c.timeline.today():
   is_cs792 = False
   is_token = False
   is_zoom = False

   if "CSE 792" in e.name.upper():
      is_cs792 = True

   try:
      if "CSE 792" in e.description.upper():
         is_cs792 = True
   except AttributeError:
      pass

   if not is_cs792:
      print(e.name)
      print("\tBegins:", e.begin)
      print("\tCSE792:", False)
      continue

   if "TOKEN" in e.name.upper():
      is_token = True

   try:
      if "TOKEN" in e.description.upper():
         is_token = True
   except AttributeError:
      pass

   etime = e.begin.timestamp()
   ename = e.name.replace('\\', '')
   ename = ename.replace('/', '')


   try:
      # print(e.description)
      m = re.findall(zoom_regex, e.description)
      zoom_link = m[0]
      is_zoom = bool(zoom_link)
      # print(zoom_link)
   except AttributeError:
      pass
   except IndexError:
      pass # match not found

   print()
   



   print(e.name)
   print("\tBegins:", e.begin)
   print("\tCSE792:", is_cs792)
   print("\tToken:", is_token)
   print("\tZoom:", is_zoom)
   if is_zoom:
      print("\tURL:", zoom_link) 

   if is_cs792 and is_token and is_zoom:
      print("\tDispatched:", True)
      executed = dispatch(etime, ename, zoom_link)
      if executed:
         break
   else:
      print("\tDispatched:", False)

   sys.stdout.flush()

for p in processes:
   p.wait()