from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException, SessionNotCreatedException, NoSuchWindowException
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from chromedriver_py import binary_path # this will get you the path variable
import time
import sys
import os 
os.environ['DISPLAY'] = ':0'

import pyautogui
import requests

import atexit
import arrow
# https://towardsdatascience.com/run-code-after-your-program-exits-with-pythons-atexit-82a0069b486a


my_name = "test"
exit_duration =  1 # mins
screenshot_rate = 0.5
start_time= int(float(sys.argv[1]))
meeting_link= sys.argv[2]
meeting_name = sys.argv[3]

print("New process")
print("\tName:",meeting_name)
print("\tStart:",start_time)
print("\tStarts:", arrow.get(start_time).humanize())
print("\tExit duration:",exit_duration, "mins")
print("\tLink:",meeting_link)
print("\tMy name:", my_name)



while start_time > time.time():
   print(f"Waiting for:", meeting_name)
   time.sleep(1)

print("Launching", meeting_name)
# sys.exit()

# https://chromedriver.chromium.org/downloads
service = Service(executable_path=binary_path)
# service = Service(executable_path='./chromedriver118')

options = webdriver.ChromeOptions()
options.add_experimental_option("detach", True)
options.add_argument("--user-data-dir=./profile2")

# try:
driver = webdriver.Chrome(service=service, options=options)
driver.implicitly_wait(10) # seconds


def say_goodbye():
   print("exiting now, goodbye:", meeting_name)
   driver.quit()

atexit.register(say_goodbye)


# except SessionNotCreatedException:
#    webdriver.quit()
#    driver = webdriver.Chrome(service=service, options=options)




def screenshot(path):
   myScreenshot = pyautogui.screenshot()
   myScreenshot.save(path)


def get_calendar(url):
   x = requests.get(url)
   print(x.status_code)
   return x.content



# driver.get("http://www.python.org")




driver.get(meeting_link)
# driver.maximize_window() 
time.sleep(3)

# button = driver.find_element_by_link_text("Sign In") 
btn = driver.find_element(By.LINK_TEXT, "Join from Your Browser")
# driver.find_element_by_xpath("// a[contains(text(), 'Join from Your Browser')]").click() 
btn.click()
time.sleep(5)


print("Setting up the meeting parameters...")
frame = driver.find_element(By.ID, "webclient")
driver.switch_to.frame(frame)

try:
   print("Joining audio...")
   audio_btn = driver.find_element(By.ID, "preview-audio-control-button")
   print("Adding name...")
   name_box = driver.find_element(By.CLASS_NAME, "preview-meeting-info-field-input")
   name_box.send_keys(my_name)
   time.sleep(2)

   # name_box.submit() 
   joint_btn = driver.find_element(By.XPATH, '//button[text()="Join"]')
   joint_btn.click()
   screenshot(f"./screenshots/{meeting_name}-{int(time.time())}.png")

except NoSuchElementException:
   print("Stuff not found")
   drive.quit()
   sys.exit(1)

print("Joining metting...")
start_time = time.time()

exit_time = start_time + (60*40)

screenshot(f"./screenshots/{meeting_name}-{int(time.time())}.png")
time.sleep(5)
screenshot(f"./screenshots/{meeting_name}-{int(time.time())}.png")

time.sleep(5)

waiting = True
while waiting:
   print("Waiting for host to allow us into the meeting...")
   try:
      time.sleep(1)
      if "We've let them know you're here" in driver.find_element(By.CLASS_NAME, "wr-tip").text:
         waiting = True
      else:
         waiting = False
   except:
      waiting = False

print("we are in.")

# while  "We've let them know you're here" in driver.page_source:
#    print("Waiting for host to allow us into the meeting...")
#    time.sleep(1)

try:
   participants_btn = driver.find_element(By.XPATH, '//span[text()="Participants"]')
   participants_btn.click()
except:
   print("Failed to click participants")

time.sleep(2)
screenshot(f"./screenshots/{meeting_name}-{int(time.time())}.png")

sys.stdout.flush()
try:
   driver.title # check if we are still open
   while time.time() < exit_time:
      time.sleep(60*screenshot_rate)
      screenshot(f"./screenshots/{meeting_name}-{int(time.time())}.png")
      driver.title # check if we are still open
      

except KeyboardInterrupt:
  pass
except NoSuchWindowException:
   print("Windows prematurely closed")
   pass
 

screenshot(f"./screenshots/{meeting_name}-{int(time.time())}.png")
driver.quit()

